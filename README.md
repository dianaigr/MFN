# MYFUTURENOW - Homework exercise

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

You don't need any requisites to run this project.

### Installing

Just clone the project for your laptop and open index.html page in a browser.

Run on a terminal:
git clone https://gitlab.com/dianaigr/MFN.git

Inside folder you will find the index.html file, and you can open it in a browser.

## Authors

* **Diana Rasteiro**
